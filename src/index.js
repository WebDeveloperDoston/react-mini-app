// import ReactDOM from "react-dom";

// const el = <h1 title="foo">Hello world</h1>;

// const container = document.querySelector("#root");

// ReactDOM.render(el, container);

// without JSX
/*
import React from "react";
import ReactDOM from "react-dom";

const el = React.createElement("h1", { title: "foo" }, "Hello");
const container = document.querySelector("#root");

ReactDOM.render(el, container);
*/

//without jsx and react

// import ReactDOM from "react-dom";

// const el = {
//   type: "h1",
//   props: {
//     title: "foo",
//     children: "Hello",
//   },
// };

// const container = document.getElementById("root");

// const node = document.createElement(el.type);

// node["title"] = el.props.title;

// const text = document.createTextNode("");

// text["nodeValue"] = el.props.children;

// node.append(text);
// container.append(node);

// with JSX & react

// import ReactDOM from "react-dom";
// import React from "react";

// const el = (
//   <div title="foo">
//     <a>Doston</a>
//     <br />
//   </div>
// );

// const container = document.getElementById("root");

// ReactDOM.render(el, container);

// without JSX

import ReactDOM from "react-dom";
import React from "react";

const el = React.createElement(
  "div",
  {
    title: "foo",
  },
  React.createElement("a", null, "bar"),
  React.createElement("br")
);

const container = document.getElementById("root");

ReactDOM.render(el, container);
