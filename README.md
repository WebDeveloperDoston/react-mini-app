# yarn commands

1. yarn (yarn install) --> npm i
2. yarn add _something_ --> npm i _something_
3. yarn start --> npm start
4. yarn build --> npm run build (run unnecessary in yarn)

# main entry file for browser

    src/index.js

# React

1. props --> aka properties
2.

```
    React.createElement(
        type,
        [props],
        [...children]
    )
```
